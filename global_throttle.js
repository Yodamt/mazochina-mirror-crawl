const { Mutex } = require('async-mutex');
const { timeout } = require('./time_utils');
const { fatalAndExit } = require('./logger');

let failures = 0;
let lastFinish = 0;

const mutex = new Mutex();

// async
module.exports = async () => {
    const release = await mutex.acquire();
    const decayPrim = Math.min(failures, 6.4);
    const expectedDelay = Math.pow(4, decayPrim) * 50;
    const unfinished = expectedDelay - (Date.now() - lastFinish);
    if (unfinished > 50) {
        await timeout(unfinished);
    }
    let updated = false;
    return {
        fail: () => {
            if (updated) { fatalAndExit('Throttle updated twice'); return; }
            updated = true;
            failures++; lastFinish = Date.now();
            release();
        },
        success: () => {
            if (updated) { fatalAndExit('Throttle updated twice'); return; }
            updated = true;
            failures = 0; lastFinish = Date.now();
            release();
        }
    };
};