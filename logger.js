const date = () => new Date().toISOString();

module.exports = {
    fail: (target, errorType, detail) => {
        console.warn('Failure - ' + errorType + ' - ' + date());
        console.warn(JSON.stringify(target), detail);
    },
    info: (target, eventType, detail) => {
        console.info('Info - ' + eventType + ' - ' + date());
        console.info(JSON.stringify(target), detail);
    },
    error: (errorType, detail) => {
        console.error('Error - ' + errorType + ' - ' + date());
        console.error(detail);
    },
    fatalAndExit: (detail) => {
        console.error('FATAL - ' + date());
        console.error(detail);
        process.exit(1);
    },
    log: (eventType, detail) => {
        console.log('Log - ' + eventType + ' - ' + date());
        console.log(detail);
    }
};